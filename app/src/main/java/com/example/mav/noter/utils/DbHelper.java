package com.example.mav.noter.utils;

/**
 * Created by mav on 05.11.2014.
 */

import android.database.sqlite.SQLiteOpenHelper;

        import android.content.Context;
        import android.database.sqlite.SQLiteDatabase;
        import android.database.sqlite.SQLiteOpenHelper;

public class DbHelper extends SQLiteOpenHelper {

    // If you change the database schema, you must increment the database version.
    public static final int DATABASE_VERSION = 1;
    public static final String DATABASE_NAME = "Noter.db";

    public static String TABLE_NAME = "NOTER";
    public static String COLUMN_ID = "_id";
    public static String TEXT_TYPE = "TEXT";
    public static String COLUMN_NAME_CREATE = "DATE";
    public static String COLUMN_NAME_TITLE = "TITLE";
    public static String COLUMN_NAME_CONTENT = "CONTENT";
    public static String COMMA_SEP = ",";
    public static String SQL_CREATE_ENTRIES = "CREATE TABLE " + TABLE_NAME + " (" +
            COLUMN_ID + " INTEGER PRIMARY KEY," +
            COLUMN_NAME_CREATE + " " +TEXT_TYPE + COMMA_SEP +
            COLUMN_NAME_TITLE + " " +TEXT_TYPE + COMMA_SEP +
            COLUMN_NAME_CONTENT + " " +TEXT_TYPE  +
            ");";

    public static String SQL_DELETE_ENTRIES = "DROP TABLE IF EXISTS " +
            TABLE_NAME;

    public DbHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // Method is called during creation of the database
    @Override
    public void onCreate(SQLiteDatabase database) {
        database.execSQL(SQL_CREATE_ENTRIES);
    }

    // Method is called during an upgrade of the database,
    // e.g. if you increase the database version
    @Override
    public void onUpgrade(SQLiteDatabase database, int oldVersion,
                          int newVersion) {
        database.execSQL(SQL_DELETE_ENTRIES);
        onCreate(database);
    }
}
