package com.example.mav.noter.ui;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.mav.noter.R;
import com.example.mav.noter.network.DownloadService;

/**
 * Created by mav on 12.11.2014.
 */
public class LoaderActivity extends Activity {
    EditText editurl;
    public static Button buttondown;
    public static ProgressBar progressBar;
    EditText filename;
    public static Intent downintent; //fixme можно сделать локальной переменной
    DownloadReciever reciever = new DownloadReciever();
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.ac_loader);

        editurl = (EditText) findViewById(R.id.url); //fixme неговорящее название для вьюхи
        buttondown = (Button) findViewById(R.id.download);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        filename = (EditText) findViewById(R.id.filename); //fixme неговорящее название для вьюхи

        registerReceiver(reciever,new IntentFilter("com.example.mav.noter.network")); //fixme вынести строку в константу. Где происходит unregister

        buttondown.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                downloadMe(editurl.getText().toString());
            }
        });
    }

    public void downloadMe(String url) {
        downintent = new Intent(getApplicationContext(), DownloadService.class);
        downintent.putExtra(DownloadService.URLN, url);
        downintent.putExtra(DownloadService.FILENAME, filename.getText().toString());
        getApplicationContext().startService(downintent);
        progressBar.setVisibility(ProgressBar.VISIBLE);
        buttondown.setClickable(false);

    }

    static void endDownloading(){ //fixme как вызвать этот метод? и возможно ли это
        progressBar.setVisibility(ProgressBar.INVISIBLE);
        buttondown.setClickable(true);

    }



    @Override
    protected void onResume() {
        super.onResume();
  //      registerReceiver(reciever,new IntentFilter("com.example.mav.noter.network"));
    }
    @Override
    protected void onPause() {
        super.onPause();
 //       unregisterReceiver(reciever);
    }


}


