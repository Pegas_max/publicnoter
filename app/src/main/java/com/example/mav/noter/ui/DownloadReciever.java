package com.example.mav.noter.ui;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.mav.noter.network.DownloadService;

/**
 * Created by mav on 17.11.2014.
 */
public class DownloadReciever extends BroadcastReceiver { // fixme пакет для класса неподходящий. Он не должен быть в ui 
    public static Context mContext; //fixme зачем эта переменная? 
    @Override
    public void onReceive(Context context, Intent intent) {
        if (mContext == null) mContext = context;

            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                String string = bundle.getString(DownloadService.FILEPATH);
                int resultCode = bundle.getInt(DownloadService.RESULT);
                if (resultCode == Activity.RESULT_OK) {
                    Toast.makeText(context, "Download was finished successfull \n" + string, Toast.LENGTH_LONG).show();
                } else {
                    Toast.makeText(context, "Что-то пошло не так...", Toast.LENGTH_LONG).show();
                }


                LoaderActivity.endDownloading();
                context.stopService(LoaderActivity.downintent);
        }
    };
}
