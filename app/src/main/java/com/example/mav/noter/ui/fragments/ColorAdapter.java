package com.example.mav.noter.ui.fragments;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.example.mav.noter.R;

import java.util.List;

/**
 * Created by mav on 11.11.2014.
 */
public class ColorAdapter extends BaseAdapter {
    static Context context; //fixme почему контекст статический?
    LayoutInflater lInflater;
    static List<String> values; //fixme название неговорящее

        public ColorAdapter(Context context, List<String> values){
            this.context = context;
            this.values = values;
            lInflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        }

    //количество элементов
    @Override
    public int getCount() {
        return values.size();
    }

    //элемент по позиции
    @Override
    public Object getItem(int position){
        return values.get(position);
    }

    // id по позиции
    @Override
    public long getItemId(int position){
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent){
       View view = convertView;
        if (view == null) {
            view = lInflater.inflate(R.layout.r_list,parent,false);
        }

        ((TextView) view.findViewById(R.id.title)).setText(values.get(position));
        // Изменение цветов
        switch (position % 3){
            case 0: {
                ((TextView) view.findViewById(R.id.title)).setBackgroundColor(Color.RED);
                break;}
            case 1: {
                ((TextView) view.findViewById(R.id.title)).setBackgroundColor(Color.BLUE);
                break;}
            case 2: {
                ((TextView) view.findViewById(R.id.title)).setBackgroundColor(Color.GREEN);
                break;}
        }

        return view;
    }
}
