package com.example.mav.noter.ui.fragments;

import android.app.Fragment;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import com.example.mav.noter.R;
import com.example.mav.noter.utils.DbHelper;

/**
 * Created by mav on 05.11.2014.
 */
public class NoteFragment extends Fragment{
    TextView time;
    TextView title;
    TextView content;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fr_note, container, false);
        time = (TextView) view.findViewById(R.id.time);
        title = (TextView) view.findViewById(R.id.title);
        content = (TextView) view.findViewById(R.id.content);
        Bundle bundle = this.getArguments();
        int id = bundle.getInt("ARG_POSITION", 0);


        DbHelper mDbHelper = new DbHelper(getActivity());
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        String[] projection = {
                DbHelper.COLUMN_ID,
                DbHelper.COLUMN_NAME_CREATE,
                DbHelper.COLUMN_NAME_TITLE,
                DbHelper.COLUMN_NAME_CONTENT
        };

        Cursor c = db.query(
                DbHelper.TABLE_NAME,  // The table to query
                projection,                               // The columns to return
                null,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                null                                 // The sort order
        );

        c.moveToPosition(id-1);
        time.setText(c.getString(1));
        title.setText(c.getString(2));
        content.setText(c.getString(3));

        return (view);
    }}
