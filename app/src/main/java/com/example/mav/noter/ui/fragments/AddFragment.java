package com.example.mav.noter.ui.fragments;

import android.app.Fragment;
import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import com.example.mav.noter.R;
import com.example.mav.noter.utils.DbHelper;

import java.util.Calendar;

/**
 * Created by mav on 05.11.2014.
 */
public class AddFragment extends Fragment {

    EditText title;
    EditText content;
    Button addbutton;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fr_add, container, false);
        title = (EditText) view.findViewById(R.id.add_title);
        content = (EditText) view.findViewById(R.id.add_cont);
        addbutton = (Button) view.findViewById(R.id.add_button);

        addbutton.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AddNote();
            }
        });
        return view;
    }

    private void AddNote (){
        addCurrent(title.getText().toString(), content.getText().toString());
        getActivity().getFragmentManager().popBackStack();
    }

    public void addCurrent(String title, String content){

        DbHelper mDbHelper = new DbHelper(getActivity());
        // Gets the data repository in write mode
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        Calendar c = Calendar.getInstance();

        int year = c.get(c.YEAR);
        int month = c.get(c.MONTH) + 1;
        int day = c.get(c.DAY_OF_MONTH);
        int hour = c.get(c.HOUR);
        int minute = c.get(c.MINUTE);
        int second = c.get(c.SECOND);

        String time = year + " " + month + "/" + day + " | " + hour + ":" + minute + ":" + second;


    // Create a new map of values, where column names are the keys
        ContentValues values = new ContentValues();
        values.put(DbHelper.COLUMN_NAME_CREATE, time);
        values.put(DbHelper.COLUMN_NAME_TITLE, title);
        values.put(DbHelper.COLUMN_NAME_CONTENT, content);

    // Insert the new row, returning the primary key value of the new row

        long newRowId;
        newRowId = db.insert(
                DbHelper.TABLE_NAME,
                null,
                values);
    }

}