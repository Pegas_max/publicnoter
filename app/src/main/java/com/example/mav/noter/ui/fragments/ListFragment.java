package com.example.mav.noter.ui.fragments;

import android.app.Fragment;
import android.app.FragmentTransaction;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;
import com.example.mav.noter.R;
import com.example.mav.noter.utils.DbHelper;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by mav on 05.11.2014.
 */



public class ListFragment extends Fragment { //fixme фрагмент с таким названием уже есть в андроид сдк, нужно стараться избегать одинаковых названий

    ListView listview;
    Button butn;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fr_list, container, false);
        listview = (ListView) view.findViewById(R.id.listview);

        butn = (Button) view.findViewById(R.id.annbtn);

        DbHelper mDbHelper = new DbHelper(getActivity()); //fixme не стоит выполнять такие действия в onCreateView
        SQLiteDatabase db = mDbHelper.getWritableDatabase();

        // Define a projection that specifies which columns from the database
        // you will actually use after this query.
        String[] projection = {
                DbHelper.COLUMN_ID,
                DbHelper.COLUMN_NAME_CREATE,
                DbHelper.COLUMN_NAME_TITLE,
                DbHelper.COLUMN_NAME_CONTENT
        };
        //Getting data from base

        String sortOrder = DbHelper.COLUMN_ID+" DESC";

        Cursor c = db.query(
                DbHelper.TABLE_NAME,  // The table to query
                projection,                               // The columns to return
                null,                                // The columns for the WHERE clause
                null,                            // The values for the WHERE clause
                null,                                     // don't group the rows
                null,                                     // don't filter by row groups
                sortOrder                                 // The sort order
        );
        List<String> fromColumns = new ArrayList<String>();

        //Making Adapter for List
        if (c.getCount() != 0) {
            c.moveToLast();
            fromColumns.add(c.getString(2));
            while (c.moveToPrevious()) {
                fromColumns.add(c.getString(2));
            }
        }
        else {
            addFirst();
        }
        ColorAdapter adapter = new ColorAdapter(getActivity(), fromColumns);
        listview.setAdapter(adapter);

        listview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                parent.getChildAt(position).setBackgroundColor(0xFFFFFF);
                showNote(position+1);
            }
        });

        butn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addNote();
            }
        });
        return view;
    }

    public void addFirst(){ //fixme неподходящее название для метода. ВЕдь мы ничего не добавляем
    Toast.makeText(getActivity(),"Добавьте свою первую запись с помощью кнопки Add New Note!",Toast.LENGTH_LONG).show(); //fixme строки нужно хранить в ресурсах, а не в коде
    }

    public void showNote(int id) {
        // Create fragment and give it an argument specifying the article it should show
        NoteFragment newFragment = new NoteFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();

        Bundle args = new Bundle();
        args.putInt("ARG_POSITION" ,id);
        newFragment.setArguments(args);

        transaction.replace(R.id.na, newFragment);
        transaction.addToBackStack("fr_list");
        // Commit the transaction
        transaction.commit();
    }

   public void addNote(){
        // Create fragment and give it an argument specifying the article it should show
        AddFragment newFragment = new AddFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.na, newFragment);

        transaction.addToBackStack("fr_list");
        // Commit the transaction
        transaction.commit();
   }

 }
