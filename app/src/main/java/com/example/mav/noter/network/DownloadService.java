package com.example.mav.noter.network;

import android.app.Activity;
import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Environment;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.io.IOException;

/**
 * Created by mav on 12.11.2014.
 */
public class DownloadService extends IntentService {

    private int result = Activity.RESULT_CANCELED;
    public static final String URLN = "urlpath";
    public static final String FILENAME = "filename";
    public static final String FILEPATH = "filepath";
    public static final String RESULT = "result";

    public DownloadService(){
        super("DownloadService");
    }

    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    @Override
    protected void onHandleIntent(Intent intent){

        String surl = intent.getStringExtra(URLN);
        String filename = intent.getStringExtra(FILENAME);
        if (isExternalStorageWritable()){
        File output = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS), filename);

        if (output.exists()) {
            output.delete();
        }
        Toast.makeText(this, "service started", Toast.LENGTH_SHORT).show();
        InputStream stream = null;
        FileOutputStream fos = null;
        try {
            URL url = new URL(surl);
            stream = url.openConnection().getInputStream();
            InputStreamReader reader = new InputStreamReader(stream);
            fos = new FileOutputStream(output.getPath());
//            fos = openFileOutput(filename, Context.MODE_WORLD_READABLE);
            int next = -1;
            while ((next = reader.read()) != -1) {
                fos.write(next);
            }
            // successfully finished
            result = Activity.RESULT_OK; //fixme почему используется именно это значение для возврата резуьтатат? Мы же не в активити

        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            if (stream != null) { // fixme поздно проверяется на налл. Что будет, если мы отдадим null в строке 57?
                try {
                    stream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (fos != null) {
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        publishResults(
                output.getAbsolutePath(),
                result);
    }}

    private void publishResults(
            String outputPath,
             int result) {
        Intent intent = new Intent("com.example.mav.noter.network");
        intent.putExtra(FILEPATH, outputPath);
        intent.putExtra(RESULT, result);
        int i = 1; //fixme зачем эта переменная?
        sendBroadcast(intent);
    }

    }

